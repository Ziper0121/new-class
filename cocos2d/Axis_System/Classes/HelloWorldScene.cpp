#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	Sprite* mario = Sprite::create("mario.png");
	mario->setPosition(200, 200);
	mario->setZOrder(1);
	this->addChild(mario);

	Sprite* mario2 = Sprite::create("mario.png");
	mario2->setPosition(200, 210);
	mario2->setZOrder(2);
	this->addChild(mario2);

    return true;
}
