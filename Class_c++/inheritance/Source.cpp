#include "Header.h"

class Lives{
private:
	int life;
	char name[100];
public:
	void setLife(int val){
		life = val;
	}
	void setName(char* val){
		if (val == nullptr)return;
		strcpy(name, val);
	}
	int getLife(){
		return life;
	}
	char* getName(){
		return name;
	}
};
class Animal : public Lives{
private:
	int bones;
	int legs;
public:
	Animal(int bones, int legs){
		this->bones = bones;
		this->legs = legs;
	}
	int getBones(){
		return bones;
	}
	int getLegs(){
		return legs;
	}
};
class Plant : public Lives{
private:
	int futaba;
public:
	Plant(int futaba){
		this->futaba = futaba;
	}
	int getFutaba(){
		return futaba;
	}
};
class Human : public Animal{
private:
	int regist_no;
public:
	Human(int regist_no) : Animal(125, 2){
		this->regist_no = regist_no;
	}
};
class Cat : public Animal{
private:
	bool tail=true;
};

int main(){
	Lives* lives;
	Animal* animal = new Animal(1,3); //자식을 부모로 바꾸는 것 가능
	lives = animal;

	//부모를 자식으로 바꾸는 오류생김
	animal = (Animal*)lives;


	return 0;
}
