#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

std::map<std::string, std::string> HelloWorld::User_Get_Id_Password_map;
std::map<std::string, std::string>::iterator HelloWorld::Id_Pass_compare;

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	//유저의 받은 아이디와 패스워드를 비교하는 맵
	HelloWorld::Id_Pass_compare 
		= HelloWorld::User_Get_Id_Password_map.begin();

	//유저가 아이디를 쓰는 곳
	HelloWorld::User_Get_Id_Write = EditBox::create
		(cocos2d::Size(300, 50), "background_textbox.png");
	HelloWorld::User_Get_Id_Write->setFontSize(30.0f);
	HelloWorld::User_Get_Id_Write->setFontColor(cocos2d::Color3B(1, 1, 0));
	HelloWorld::User_Get_Id_Write->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2.0f));
	this->addChild(HelloWorld::User_Get_Id_Write);

	//유저의 비밀번호를 쓰는 곳
	HelloWorld::User_Get_Password_Write = EditBox::create
		(cocos2d::Size(300, 50), "background_textbox.png");
	HelloWorld::User_Get_Password_Write->setFontSize(30.0f);
	HelloWorld::User_Get_Password_Write->setFontColor(cocos2d::Color3B(1, 1, 0));
	HelloWorld::User_Get_Password_Write->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2.0f - 60.0f));
	this->addChild(HelloWorld::User_Get_Password_Write);

	FILE* ID_Nuver_FILE = fopen("ID_Nuver.txt", "r");
	char nuver = fgetc(ID_Nuver_FILE);
	std::stringstream ID_Nuver;
	ID_Nuver << "ID_Nuver_" << nuver << ".txt";
	FILE* User_ID_FILE = fopen(ID_Nuver.str().c_str(), "w");

	//로그인 버튼
	HelloWorld::Login_Button = Button::create
		("btn_login_up.png", "btn_login_down.png");
	HelloWorld::Login_Button->setSwallowTouches(false);
	HelloWorld::Login_Button->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2 - 130));
	this->addChild(HelloWorld::Login_Button);

	//회원 가입 버튼
	HelloWorld::Sing_Up_Button = Button::create
		("background_textbox.png");
	HelloWorld::Sing_Up_Button->setSwallowTouches(false);
	HelloWorld::Sing_Up_Button->setTitleText("Sing_Up");
	HelloWorld::Sing_Up_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	HelloWorld::Sing_Up_Button->setPosition(cocos2d::Vec2(960.0f / 1.5f, 640.0f / 2 - 130));
	this->addChild(HelloWorld::Sing_Up_Button);

	//login을 실행시키는 람다
	auto Lamda_Maus_Touch = cocos2d::EventListenerTouchOneByOne::create();
	Lamda_Maus_Touch->onTouchBegan = std::bind(&HelloWorld::HelloWorld_go_Article_Liset_touch,
		this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Lamda_Maus_Touch, this);

	return true;
}
bool HelloWorld::HelloWorld_go_Article_Liset_touch(cocos2d::Touch* Tch, cocos2d::Event* Et){
	Rect Button_Login = Login_Button->getBoundingBox();
	Rect Button_Sing_Up = Sing_Up_Button->getBoundingBox();
	if (Button_Login.containsPoint(Tch->getLocation())){
		char* get_Write_ID = (char*)HelloWorld::User_Get_Id_Write->getText();
		char* get_Write_PassWord = (char*)HelloWorld::User_Get_Password_Write->getText();
		for (; Id_Pass_compare != User_Get_Id_Password_map.end(); Id_Pass_compare++){
			// 아이디와 비밀번호가 하나라도 있는지를 확인한다. (비어 있을 경우 참을 반환)
			if (User_Get_Id_Password_map.empty()){
				CCLOG("YOU CAN NOT DO IT LOGIN");
				break;
			}
			//비어 있지 않을 경우 비교를 한다.
			else if (strcmp(get_Write_ID, HelloWorld::Id_Pass_compare->first.c_str()) &&
				strcmp(get_Write_PassWord, HelloWorld::Id_Pass_compare->second.c_str())){
				Director::getInstance()->replaceScene(Article_List::Article_List_Scene());
			}
			Director::getInstance()->replaceScene(Article_List::Article_List_Scene());
		}
	}
	if (Button_Sing_Up.containsPoint(Tch->getLocation())){
		Director::getInstance()->replaceScene(Sing_Up::Sing_Up_scene());
	}
	return true;
}