#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "Scene2.h"
USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	btn_1 = cocos2d::Sprite::create("arrow.png");
	btn_1->setPosition(400, 400);
	this->addChild(btn_1);

	//cocos2d::EventListenerTouchOneByOne::create()는 클릭값을 
	auto evt_1 = cocos2d::EventListenerTouchOneByOne::create();
	// onTouchBegan와 묶는다. this는 자기자신 class를 봐라  이것 std::placeholders::_1, std::placeholders::_2파라메터ㄴ
	evt_1->onTouchBegan = std::bind(&HelloWorld::touchBegan_1, this, std::placeholders::_1, std::placeholders::_2);
	// evt_1의 이벤트를 this의 지금 씬(지금의 클래스) addEventListenerWithSceneGraphPriority맨우에 화면을 터치한다
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(evt_1, this);

    return true;
}

bool HelloWorld::touchBegan_1(cocos2d::Touch* t, cocos2d::Event* _evt){

	//Rect는 사각형이다. 
	Rect rect = btn_1->getBoundingBox();

	//rect.containsPoint는 이곳에 값이 (t->getLocation())터치한 좌표값과 같은가 ?
	if (rect.containsPoint(t->getLocation())){
		//getInstance()(MyLayer::createScene_1()를 실행을 해야한다.
		Director::getInstance()->replaceScene(MyLayer::createScene_1());
	}
	return true;
}
