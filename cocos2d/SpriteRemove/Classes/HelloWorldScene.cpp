#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	//새에다가 숫자를 입력해 놓는 것

	for (int i = 0; i < 10; i++){
		for (int k = 0; k < 10; k++){

			bird[i][k] = Sprite::create("bird.png");
			bird[i][k]->setPosition(30 + (i * 100), 30 + (k * 63));
			this->addChild(bird[i][k]);

			std::stringstream sstr;
			sstr << k;
			sstr << i;
			std::string result = sstr.str();

			cocos2d::Label * num = cocos2d::Label::createWithTTF(sstr.str(), "04B_21__.TTF", 10.0f);
			num->setPosition(10.0, 0);
			bird[i][k]->addChild(num);
		}
	}

	auto touch = EventListenerTouchOneByOne::create();
	touch->onTouchBegan = std::bind(&HelloWorld::ccTouchBegan, this,
		std::placeholders::_1,
		std::placeholders::_2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touch, this);
    return true;
}
bool HelloWorld::touchbegen(cocos2d::Touch* touch, cocos2d::Event* evt){
	for (int i = 0; i < 10; i++){
		for (int k = 0; k < 10; k++){
			if (bird[i][k] == nullptr) continue;

			cocos2d::Rect rect = bird[i][k]->getBoundingBox();
			if (rect.containsPoint(touch->getLocation())){
				bird[i][k]->removeFromParent();
				bird[i][k] = nullptr;
			}
		}
	}
	return true;
}