#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	/*          변수          */
	//버섯
	cocos2d::Sprite* mushroom;

	//애니메이션
	void restAnimation();

	float force = 0.0;

	//왼쪽
	bool bool_Keyboard_Left = false;
	//오른쪽		
	bool bool_Keyboard_Right = false;
	//점프		
	bool bool_Keyboard_Space = false;

	static cocos2d::Animation* Ani_mush;

	/*          함수          */

	//키를 눌렀을 때
	void bool_Keyboard_down(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et);
	//키를 땟을 때
	void bool_Keyboard_up(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et);

	//움직이는 버섯
	void logic(float dt);
	void jumplogic(float dt);

};

#endif // __HELLOWORLD_SCENE_H__
