#include <stdio.h>
#include <stdlib.h>

class ClassTest{
private:
	char* name;
public :
	ClassTest(){
		printf("name 변수 malloc,1000mbyte\n");
		name = (char*)malloc(sizeof(char) *100);
	}
	virtual~ClassTest(){
		free(stdin);
		printf("ClassTest 소멸자 호출\n");
	}
};
class ClassTestchild : public ClassTest{
private:
	char* status;
public:
	ClassTestchild(){
		status = (char*)malloc(sizeof(char) * 100);
		printf("cassTestchild 의 생성자 호출\n");
	}
	virtual~ClassTestchild(){
		free(stdin);
		printf("ClassTestchild 소멸자 호출\n");
	}
};
int main(){
	ClassTest* instance;
	instance = new ClassTestchild();
}