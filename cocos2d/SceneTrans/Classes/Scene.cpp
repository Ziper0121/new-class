#include "Scene2.h"

cocos2d::Scene* MyLayer::createScene_1(){

	cocos2d::Scene* scene_1 = cocos2d::Scene::create();
	MyLayer* layer_1 = MyLayer::create();
	scene_1->addChild(layer_1);

	return scene_1;
}
bool MyLayer::init(){
	//메모리 관리에 넣어주는 것
	if (!Layer::init())return false;

	//50.0f는 크기를 말한다
	cocos2d::Label* label_1 = cocos2d::Label::createWithTTF("Scene2", "04b20.ttf", 50.0f);
	label_1->setPosition(400, 300);

	//this 는 마이레이러
	this->addChild(label_1);

	return true;
}
