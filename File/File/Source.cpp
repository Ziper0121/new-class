#define T2
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <ostream>
#include <istream>

#ifdef T1

int main(){
	int somInteger = 29;
	float someFloat = 83.2994; 
	FILE* outfile = fopen("output.txt", "w");
	fprintf(outfile, "file output hello~\n");
	fprintf(outfile, "someInteger:%d\n", somInteger);
	fprintf(outfile, "someFloat:%d\n", someFloat);
	fclose(outfile);
	FILE* infile = fopen("output.txt", "r");
	while (true){
		char c = fgetc(infile);
		if (feof(infile)){
			break;
		}
		printf("%c", c);
	}
	fgetc(stdin);
	return 0;
}
#endif 

#ifdef T2

int main(){
	std::ifstream ifstr = std::ifstream("test.txt", std::ios::in);
	
	while (true){
		char c = ifstr.get();
		if (ifstr.eof()) break;
		printf("%c", c);
	}
	ifstr.clear();
	ifstr.seekg(0, std::ios::beg);
	while (true)
	{
		char buff[100];
		ifstr.getline(buff, 100);
		if (ifstr.eof())break;
		printf("%s", buff);
	}
	ifstr.clear();
	ifstr.close();

	return 0;
}
#endif

#ifdef T3

int main(){

	return 0;
}
#endif