#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#define PI 3.141592
#define DEG_90 (PI * 0.5f)
#define DEG_180 PI
#define DEG_270 (PI * 1.5f)
#define DEG_360 (PI * 2.0f)

#include "cocos2d.h"
#include "AppDelegate.h"

using namespace cocos2d;

//== 파괴해야할 블록들에 대한 상수
#define BLOCK_HORIZONTAL_MAX 14
#define BLOCK_VERTICAL_MAX 6

class HelloWorld : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);

	//게임의 상태를 알려주는 이넘
	enum GameState{ READY, PLAYING, WIN, LOSE };
private:
	//==플레이어가 시작될 최초 위치 지정
	const Vec2 PLAYER_INIT_POS = Vec2(AppDelegate::displaySize.width / 2, 8 * 4);

	//==플레이어가 이동할 델타값
	const Vec2 PLAYER_MOVING_DT = Vec2(200, 0);

	//==현재 타일의 크기
	const float tileWidth = 8.0f;

	// == 공의 최초 속도
	const float INIT_SPEED = 50.0f;


	// == 공의 공격력
	const int ball_Damage = 3;

	//==keyboard Event Listener
	EventListenerKeyboard* keybd = nullptr;

	//==keyboard Event var
	bool leftArrow = false;
	bool rightArrow = false;
protected:
	//== 플레이어가 될 Sprite
	Sprite* player = nullptr;

	// == 공의 컨테인 포인트
	Vec2 ball_contanBox[360];

	//==떠다닐 공이 될 Sprite
	Sprite* ball = nullptr;

	// == 공의 나아갈 방향을 나타낼 Vector
	Vec2 ballDirection = Vec2(0, 0);
	float ballAngle = 0.0f;
	float ballSpeed = INIT_SPEED;

	// == 파괴해야할 블록들
	Sprite* blocks[BLOCK_VERTICAL_MAX][BLOCK_HORIZONTAL_MAX];

	// == 블록의 체력을 설정, 0이 되면 삭제
	int blockHealth[BLOCK_VERTICAL_MAX][BLOCK_HORIZONTAL_MAX];

	//==게임의 현재 상태를 담는 변수 (enum)
	GameState state = GameState::READY;
public:
	void createPlayerSprite();
	void createBallSprtie();
	void createBlocks();
	void registKeyboardEvent();

	void makeDirectionVectorByAngle(float speed, float angle);

	// == 플레이어 키보드 좌우 이동 처리 스케줄러
	void schedulePlayerLogic(float dt);

	// == 게임 상태별 공 이동 처리 스케줄러
	void scheduleBallMoving(float dt);

	// == 플레이어 처리 스케줄러
	void starGane();
	void loseGame();
	void winGame();

	// == State 를 변동한다.
	void setStatePlaying();
	void setStateReady();
	void setStateLose();
	void setStateWin();

	// == 윈에 닿게 하기
	bool draWcircle(Vec2 ballPos, int segment, int i, int k);

	//== 블럭의 위치와 색을 지정 해준다.
	void reinforce_blocks(Sprite* Blocks_Sprite, char* Blocks_color, int HP, int x, int y);

	//== 블럭의 사라지는 이벤트를 만들어주는 함수
	void destroyBlock(int i, int k, char* now, char* after);
};

#endif // __HELLOWORLD_SCENE_H__
