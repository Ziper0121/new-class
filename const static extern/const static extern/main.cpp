
#include "Header.h"
#include <cstdio>

int main(){


	/*값은 같다 상수이다.*/
	print_const_integer();
	//const는 프로그램이 끝이 날때 까지 변하지 않는다.
	printf("main.cpp > CONST_INTEGER : %d \n", CONST_INTEGER);
	ConstExampleClass get_const = ConstExampleClass();
	//class내부의 const값
	printf("%d\n",get_const.CONST_INTEGER);

	/*static 변수는 전역변수이다.*/
	//외부선언된 static변수
	print_static_integer();
	//class내부의 static변수
	print_class_static_integer();
	//const의 값
	printf("main.cpp > CONST_INTEGER : %d \n\n\n",CONST_INTEGER);

	//main의 extern을 나타낸다. 
	printf("main.cpp > externInteger : %d\n", externInteger);
	//몸체의 extern의 값을 반환한다.
	print_extern_integer();
	//extern의 값을 1로 초기화 한다.
	externInteger = 1;
	//main의 extern 값을 리턴한다.
	printf("main.cpp > externInteger : %d\n", externInteger);
	// 몸체의 extern의 값을 반환한다.
	print_extern_integer();

	return 0;
}