
/*const변수의 이해*/
const int CONST_INTEGER = 25;
//외부 const의 값을 부르는 변수
void print_const_integer();

class ConstExampleClass{
public:
	const int CONST_INTEGER = 30;
	char* returnString_1();
	const char* returnString_2();
};

/*Static변수의 이해*/
static int staticInteger;
class StaticExampleClass{
public:
	static int staticInteger;
	int integer;
	static void staticFunction();
};
//외부선언된 static변수
void print_static_integer();
//class내부의 static변수
void print_class_static_integer();

/*estern변수의 이해*/
extern int externInteger;
extern "C"{
	/*c++ Name magling을 사용하지 않고, C Name Decoration을 사용하겠다는 구문*/
}
//몸체의 extern의 값을 반환한다.
void print_extern_integer();