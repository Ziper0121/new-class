#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
	}
    return true;
}
void HelloWorld::horizontal_test(){
	Layout* horizontal_layout = Layout::create();
	horizontal_layout->setLayoutType(LayoutType::VERTICAL);
	horizontal_layout->setSize(Size(400, 300));
	horizontal_layout->setBackGroundColor(Color3B(80, 0, 0));
	horizontal_layout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	addChild(horizontal_layout);

	Button* btn1 = Button::create("btn_up.png");
	btn1->setTitleText("btn1");
	horizontal_layout->addChild(btn1);

	auto param1 = LinearLayoutParameter::create();
	param1->setMargin(Margin(0, 0, 0, 50));
	param1->setGravity(LinearLayoutParameter::LinearGravity::CENTER_HORIZONTAL);

	btn1->setLayoutParameter(param1);

	Button* btn2 = Button::create("btn_up.png");
	btn2->setTitleText("btn2");
	horizontal_layout->addChild(btn2);

	auto param2 = LinearLayoutParameter::create();
	param2->setGravity(LinearLayoutParameter::LinearGravity::RIGHT);

	btn1->setLayoutParameter(param2);

}
void HelloWorld::Relative_Layout(){
	Layout* relative_layout = Layout::create();
	relative_layout->setLayoutType(LayoutType::RELATIVE);
	relative_layout->setSize(Size(960, 640));
	relative_layout->setBackGroundColor(Color3B(80, 0, 0));
	relative_layout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	addChild(relative_layout);

	Button* btn1 = Button::create("btn_up.png");
	auto param1 = RelativeLayoutParameter::create();
	param1->setAlign(RelativeAlign::CENTER_IN_PARENT);
	param1->setRelativeName("adcty");
	btn1->setLayoutParameter(param1);
	btn1->setTitleText("btn1");
	relative_layout->addChild(btn1);

	Button* btn2 = Button::create("btn_up.png");
	auto param2 = RelativeLayoutParameter::create();
	param2->setAlign(RelativeAlign::PARENT_TOP_RIGHT);
	param2->setAlign(RelativeAlign::LOCATION_ABOVE_LEFTALIGN);
	param2->setRelativeToWidgetName("abcty");
	btn2->setLayoutParameter(param2);
	btn2->setTitleText("btn2");
	relative_layout->addChild(btn2);
}
void HelloWorld::Scale9_Sprite(){
	Scale9Sprite* scale9sprite1 = Scale9Sprite::create("scale9test.png");
	scale9sprite1->setScale9Enabled(true);
	scale9sprite1->setScale(2.0f);
	scale9sprite1->setPosition(200, 200);
	addChild(scale9sprite1);

	Scale9Sprite* scale9sprite2 = Scale9Sprite::create("scale9test.png");
	scale9sprite2->setScale9Enabled(true);
	scale9sprite2->setContentSize(Size(200, 200));
	scale9sprite2->setPosition(200, 420);
	addChild(scale9sprite2);

	Scale9Sprite* scale9sprite3 = Scale9Sprite::create("scale9test2.png");
	scale9sprite3->setScale9Enabled(true);
	scale9sprite3->setContentSize(Size(200, 200));
	scale9sprite3->setPosition(450, 200);
	addChild(scale9sprite3);

	Scale9Sprite* scale9sprite4 = Scale9Sprite::create("scale9test2.png",
		Rect(0,0,100,100),
		Rect(50,22,27,44));
	scale9sprite4->setScale9Enabled(true);
	scale9sprite4->setContentSize(Size(200, 200));
	scale9sprite4->setPosition(450,420);
	addChild(scale9sprite4);
}
void HelloWorld::Layout_text(){
	Layout* test = Layout::create();
	test->setLayoutType(Layout::Type::HORIZONTAL);
	test->setSize(Size(800, 400));
	test->setBackGroundColor(Color3B(80, 0, 0));
	test->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	addChild(test);

	Button* btn1 = Button::create("btn_up.png");
	btn1->setTitleText("btn1");
	test->addChild(btn1);

	Button* btn3 = Button::create("btn_up.png");
	btn3->setTitleText("btn3");
	test->addChild(btn3);

	Button* btn4 = Button::create("btn_up.png");
	btn4->setTitleText("btn4");
	test->addChild(btn4);

	Button* btn5 = Button::create("btn_up.png");
	btn5->setTitleText("btn5");
	test->addChild(btn5);
}