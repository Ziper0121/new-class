#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	cocos2d::Sprite* mario = nullptr;
	cocos2d::Sprite* bird = nullptr;

	cocos2d::Label* mairo_print = nullptr;
	cocos2d::Label* bird_print = nullptr;
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	//시간을 이요하는 함수
	void logic(float dt);

	//시간을 알려주는 것
	float time = 0;
	//지금의 컬러 상태
	int state = 0;
};

#endif // __HELLOWORLD_SCENE_H__
