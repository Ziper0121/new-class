#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

#define FORCE 200
using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	HelloWorld::KoreanFlag();
	return true;
}
void HelloWorld::draWCircle(){
	DrawNode* node = DrawNode::create();
	Vec2 center(200, 200);
	float radius = 50.0f;
	float angle = 24.0f;
	int segments = 12;
	bool draWLineToCenter = true;
	node->drawCircle(center, radius, angle, segments, draWLineToCenter, Color4F(1.0f, 1.0f, 1.0f, 1.0f));
	addChild(node);
}
void HelloWorld::draWpoly(){
	DrawNode* node = DrawNode::create();
	Vec2 poli[4];
	int numverOfPoints = 4;
	bool closePolygon = true;
	poli[0] = Vec2(400, 400);
	poli[1] = Vec2(300, 300);
	poli[2] = Vec2(260, 122);
	poli[3] = Vec2(400, 250);
	node->drawPoly(poli, numverOfPoints, closePolygon, Color4F(1.0f, 1.0f, 0.0f, 1.0f));
	addChild(node);
}

void HelloWorld::grid(){
	DrawNode* line = DrawNode::create();
	Vec2 Line_up;
	Vec2 Line_Dawn;

	Line_up.x = 10;
	Line_up.y = 0;

	Line_Dawn.x = 10;
	Line_Dawn.y = 650;

	Vec2 Line_right;
	Vec2 Line_left;
	Line_left.x = 0;
	Line_left.y = 10;

	Line_left.x = 960;
	Line_left.y = 10;

	for (int i = 0; i < 100; i++){
		Line_up.x = 10 + i*10;
		Line_Dawn.x = 10 + i*10;
		line->drawLine(Line_up, Line_Dawn, Color4F(1.0f, 1.0f, 1.0f, 1.0f));
		Line_right.y = 10 + i * 10;
		Line_left.y = 10 + i * 10;
		line->drawLine(Line_right, Line_left, Color4F(1.0f, 1.0f, 1.0f, 1.0f));
	}
	addChild(line);
}
void HelloWorld::samgag(){
	DrawNode* node = DrawNode::create();
	Vec2 origin(250, 0);
	for (int i = 0; i < 360; i++){
		Vec2 two;
		/*
		반전
		다른 방법은 
		two.x = 250.0f *cos((double)i *-3.141592 / 180 );
		two.y = 250.0f *sin((double)i *-3.141592 / 180);
		또는
		색깔을 
		node->drawSolidPoly(Poli, 3, Color4F(1-(i / 360.0f), 0, 0, 1.0f));
		이런 식으로
		*/
		two.x = 250.0f *cos((double)i *3.141592 / 180 );
		two.y = 250.0f *sin((double)i *3.141592 / 180 );

		Vec2 Poli[3];
		Poli[0] = origin;
		Poli[1] = two;
		Poli[2] = Vec2(0, 0);
		node->drawSolidPoly(Poli, 3, Color4F(i / 360.0f, 0, 0, 1.0f));
		origin = two;
	}

	node->setPosition(300, 300);
	addChild(node);
}
void HelloWorld::triangle(){
	/*DrawNode* node = DrawNode::create();
	Vec2 position[3];

	for (int i = 0; i < 3; i++){
		position[0] = Vec2(200+(i*78), 200+(i*80));
		position[1] = Vec2(280 + (i * 78), 280+(i * 80));
		position[2] = Vec2(360 + (i * 78), 200+(i * 80));
		node->drawSolidPoly(position, 3, Color4F(1.0, 0.0, 0.0, 1.0));
	}

	node->setPosition(100,100);
	addChild(node);*/

	DrawNode* node = DrawNode::create();

	node->drawCircle(Vec2(500, 500), 40, 90 * 3.14192 / 180, 3, false, Color4F(1, 1, 1, 1));
	node->drawCircle(Vec2(465, 440), 40, 90 * 3.14192 / 180, 3, false, Color4F(1, 1, 1, 1));
	node->drawCircle(Vec2(535, 440), 40, 90 * 3.14192 / 180, 3, false, Color4F(1, 1, 1, 1));

	node->drawCircle(Vec2(430, 380), 40, 90 * 3.14192 / 180, 3, false, Color4F(1, 1, 1, 1));
	node->drawCircle(Vec2(500, 380), 40, 90 * 3.14192 / 180, 3, false, Color4F(1, 1, 1, 1));
	node->drawCircle(Vec2(570, 380), 40, 90 * 3.14192 / 180, 3, false, Color4F(1, 1, 1, 1));
	addChild(node);

}
void HelloWorld::square(){
	DrawNode* node = DrawNode::create();
	Vec2 poli[6];
	for (int i = 0; i < 6; i++){
		poli[i] = Vec2(100 * cos(50 * i*3.1415 / 180), 100 * sin(50 * i*3.1415 / 180));
	}

	node->drawPoly(poli, 6, true, Color4F(1, 0, 0, 1));
	node->setPosition(200, 200);
	addChild(node);

}
void HelloWorld::KoreanFlag(){
	DrawNode* node = DrawNode::create();
	Vec2 origin(200, 0);
	for (int i = 0; i < 361; i++){
		Vec2 two;
		two.x = 200.0f *cos((double)i * 3.1415 / 180);
		two.y = 200.0f *sin((double)i * 3.1415 / 180);

		Vec2 poli[3];
		// 전의 루프의 origin을 origin
		poli[0] = origin;
		// 1도 올라가는 지점
		poli[1] = two;
		//중간 지점
		poli[2] = Vec2(0, 0);
		if (i < 182)
			node->drawSolidPoly(poli, 3, Color4F(1.0f, 0, 0, 1));
		else
			node->drawSolidPoly(poli, 3, Color4F(0, 0, 1.0f, 1));
		//지금 origin의 poli로 받는 것 
		origin = two;
	}

	//노드는 상대적이다?
	node->drawSolidCircle(Vec2(105, 0), -105, 0, 50, Color4F(0, 0, 1, 1));
	node->drawSolidCircle(Vec2(-105, 0), 105, 0, 50, Color4F(1, 0, 0, 1));

	node->setPosition(500, 500);

	addChild(node);


}

void HelloWorld::ohmanseong(){
	DrawNode* node = DrawNode::create();
	Vec2 origin_2[5];
	int k = 2;
	Vec2 origin(100, 0);
	for (int i = 0; i < 366; i++){
		Vec2 two;
		if (i < 5){
			origin_2[i].x = 100.0f *cos((double)72 * i * 3.1415 / 180);
			origin_2[i].y = 100.0f *sin((double)72 * i *3.1415 / 180);

		}

		two.x = 100.0f *cos((double) i * 3.1415 / 180);
		two.y = 100.0f *sin((double) i *3.1415 / 180);

		node->drawLine(origin, two, Color4F(1, 1, 1, 1));
		origin = two;
	} 

	node->drawLine(origin_2[0], origin_2[2], Color4F(1, 1, 1, 1));
	node->drawLine(origin_2[1], origin_2[3], Color4F(1, 1, 1, 1));
	node->drawLine(origin_2[2], origin_2[4], Color4F(1, 1, 1, 1));
	node->drawLine(origin_2[3], origin_2[0], Color4F(1, 1, 1, 1));
	node->drawLine(origin_2[4], origin_2[1], Color4F(1, 1, 1, 1));

	node->setPosition(200, 200);
	addChild(node);
}
void HelloWorld::drawCircle(){
	DrawNode* DarawCircle = DrawNode::create();
	Vec2 origin(50, 0);
	for (int i = 0; i < 500; i++){
		Vec2 two;
		two.x = 50 * cos((double)i * 3.1415 / 180);
		two.y = 50 * sin((double)i * 3.1415 / 180);
		DarawCircle->drawLine(origin, two, Color4F(1, 1, 1, 1));
		origin = two;
	}
	DarawCircle->setPosition(200, 200);
	addChild(DarawCircle);
}