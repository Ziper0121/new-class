#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	HelloWorld::mushroom = cocos2d::Sprite::create("stand.png");
	HelloWorld::mushroom->setAnchorPoint(Vec2(0.5f, 0.0f));
	mushroom->setPosition(cocos2d::Vec2(300, 300));
	this->addChild(mushroom);

	auto Tuch_KeyBoard = cocos2d::EventListenerKeyboard::create();
	Tuch_KeyBoard->onKeyPressed = std::bind(&HelloWorld::bool_Keyboard_down, this, std::placeholders::_1, std::placeholders::_2);
	Tuch_KeyBoard->onKeyReleased = std::bind(&HelloWorld::bool_Keyboard_up, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Tuch_KeyBoard, this);

	this->schedule(schedule_selector(HelloWorld::logic));
	this->schedule(schedule_selector(HelloWorld::jumplogic));

	HelloWorld::mushroom->runAction(RepeatForever::create(Animate::create(Ani_mush)));

    return true;
}
void HelloWorld::restAnimation(){
	Sprite* mario = Sprite::create("mario1.png");
	Animation* anim = Animation::create();
	anim->addSpriteFrameWithFile("mario1.png");
	anim->addSpriteFrameWithFile("mario2.png");
	anim->addSpriteFrameWithFile("mario3.png");
	anim->addSpriteFrameWithFile("mario2.png");
	anim->setDelayPerUnit(0.1f);
	mario->setPosition(180, 320);
	addChild(mario);

	mario->runAction(RepeatForever::create(Animate::create(anim)));
}

void HelloWorld::bool_Keyboard_down(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et){
	HelloWorld::mushroom->stopAllActions();
	//오른쪽
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		HelloWorld::bool_Keyboard_Right = true;
		HelloWorld::mushroom->setFlippedX(true);
		Ani_mush->addSpriteFrameWithFile("run1.png");
		Ani_mush->addSpriteFrameWithFile("run2.png");
		Ani_mush->addSpriteFrameWithFile("run3.png");
		Ani_mush->addSpriteFrameWithFile("run2.png");
	}
	//왼쪽
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		HelloWorld::bool_Keyboard_Left = true;
		HelloWorld::mushroom->setFlippedX(false);
		Ani_mush->addSpriteFrameWithFile("run1.png");
		Ani_mush->addSpriteFrameWithFile("run2.png");
		Ani_mush->addSpriteFrameWithFile("run3.png");
		Ani_mush->addSpriteFrameWithFile("run2.png");
	}
	/*this->runAction(
			Sequence::create(
			DelayTime::create(0.2f),
			CallFunc::create([world]()->void{world->force = 30.0f; }
		), nullptr));
		*/
	//점프
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_SPACE){
		HelloWorld* World = HelloWorld::create();
		World->runAction(Sequence::create
			(DelayTime::create(0.1f),
			CallFunc::create([World]()->void{World->force = 30.0f; }
		),nullptr));
		Ani_mush->addSpriteFrameWithFile("jump.png");
	}
	Ani_mush->setDelayPerUnit(0.3f);
}
void HelloWorld::bool_Keyboard_up(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et){
	HelloWorld::mushroom->stopAllActions();
	//오른쪽
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		HelloWorld::bool_Keyboard_Right = false;
		HelloWorld::mushroom->setTexture("stand.png");
	}
	//왼쪽
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		HelloWorld::bool_Keyboard_Left = false;
		HelloWorld::mushroom->setTexture("stand.png");
	}

}
void HelloWorld::logic(float dt){
	cocos2d::Vec2 pos = HelloWorld::mushroom->getPosition();
	if (HelloWorld::bool_Keyboard_Left){
		pos.x = pos.x - dt*200;
	}
	if (HelloWorld::bool_Keyboard_Right){
		pos.x = pos.x + dt*200;
	}
	HelloWorld::mushroom->setPosition(pos);
}

void HelloWorld::jumplogic(float dt){
	cocos2d::Vec2 pos = HelloWorld::mushroom->getPosition();// get 가져오다 set 집어넣다
	force -= (dt*200);
	pos.y = pos.y + force;
	if (pos.y >= 300.0f){
		HelloWorld::mushroom->setPosition(pos);
	}
	else if (pos.y < 300.0f && HelloWorld::mushroom->getPosition().y != 300.0f){
		HelloWorld::mushroom->setPosition(pos.x, 300.0f);
	}
}