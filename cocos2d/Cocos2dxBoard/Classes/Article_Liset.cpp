#include "Article_List.h"

using namespace cocos2d::ui;

cocos2d::Scene* Article_List::Article_List_Scene(){
	cocos2d::Scene* List_of = cocos2d::Scene::create();
	Article_List* P_Article_List = Article_List::create();
	List_of->addChild(P_Article_List);
	return List_of;
}

std::vector<std::string> Article_List::Article_name;
std::vector<std::string> Article_List::Article_in;

bool Article_List::init(){
	if (!Layer::init()) return false;

	//로그인 이동
	Article_List::Login_Back_Button = cocos2d::ui::Button::create
		("background_textbox.png");
	Article_List::Login_Back_Button->setPosition(cocos2d::Vec2(300, 100));
	Article_List::Login_Back_Button->setSwallowTouches(false);
	Article_List::Login_Back_Button->setTitleText("Bake");
	Article_List::Login_Back_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	this->addChild(Article_List::Login_Back_Button);

	for (int i = 0; i < Article_List::Article_name.size(); i++){
		Article_List::Button_make.push_back(Button::create("background_textbox.png"));
		Article_List::Button_make.at(i)->setPosition(cocos2d::Vec2(500, (i + 1) * 150));
		Article_List::Button_make.at(i)->setSwallowTouches(false);
		Article_List::Button_make.at(i)->setTitleText(Article_List::Article_name.at(i).c_str());
		Article_List::Button_make.at(i)->setTitleColor(cocos2d::Color3B(1, 1, 0));
		this->addChild(Article_List::Button_make.at(i));
	}

	//글 쓰는 곳
	Article_List::Writing_Go_Button = cocos2d::ui::Button::create
		("background_textbox.png");
	Article_List::Writing_Go_Button->setPosition(cocos2d::Vec2(700, 100));
	Article_List::Writing_Go_Button->setSwallowTouches(false);
	Article_List::Writing_Go_Button->setTitleText("Writing");
	Article_List::Writing_Go_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	this->addChild(Article_List::Writing_Go_Button);

	auto Lamda_Tuch_Button = cocos2d::EventListenerTouchOneByOne::create();
	Lamda_Tuch_Button->onTouchBegan = std::bind(&Article_List::Article_Bach_Touch,
		this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority
		(Lamda_Tuch_Button, this);

	return true;
}
bool Article_List::Article_Bach_Touch(cocos2d::Touch* Tch, cocos2d::Event* Et){
	cocos2d::Rect Login_Back_Rect = Article_List::Login_Back_Button->getBoundingBox();
	cocos2d::Rect Writing_Go_Button_Rect = Article_List::Writing_Go_Button->getBoundingBox();
	for (int i = 0; i < Article_List::Article_name.size(); i++){
		cocos2d::Rect Button_make_Rect = Article_List::Button_make[i]->getBoundingBox();
		if (Button_make_Rect.containsPoint(Tch->getLocation())){
			cocos2d::Director::getInstance()->replaceScene(Read_Article::Read_Article_scene());
		}
	}
	if (Login_Back_Rect.containsPoint(Tch->getLocation())){
		cocos2d::Director::getInstance()->replaceScene(HelloWorld::createScene());
	}
	if (Writing_Go_Button_Rect.containsPoint(Tch->getLocation())){
		cocos2d::Director::getInstance()->replaceScene(Writing::Writing_Scene());
	}
	return true;
}