#include <functional>

typedef std::function<float(float a, float b)> tFunc_div;
void some_function(){
	printf("this is some function\n");
}

int adder(int param1, int param2){
	return param1 + param2;
}

float divider(float a, float b){
	return a / b;
}

class SomeClass{
public:
	void classFunction(int a, float b){
		printf("someClass: classFunction(%d, %f)\n", a, b);
	}
};
void init_function(){
	std::function<void()> function1 = std::bind(some_function);
	std::function<int(int param1, int param2)> function2 =
		std::bind(adder, std::placeholders::_1, std::placeholders::_2);
	some_function();
	int result = function2(1, 2);
	printf("result:%d\n", result);
	tFunc_div function3 = std::bind(divider, std::placeholders::_1, std::placeholders::_2);
	SomeClass* tergetInstance = new SomeClass();
	std::function<void(int a, float b)> functionInClass = std::bind(&SomeClass::classFunction, tergetInstance, std::placeholders::_1, std::placeholders::_2);
	functionInClass(10, 20.3);
}
int main(){
	init_function();

	std::function<void()> function_empty;

	return 0;
}
/*bin은 빌리다 ? 
STL이란
템플린 규격
다양한 타입을 통해 합수나 클래스를 생성할 수 있게 해주는 테크닉
존재여부를 모르고 사용하는 타입?
속해 있는 것들은 클래스를 모두 복사한다.
*/