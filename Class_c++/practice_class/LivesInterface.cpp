#include "LivesInterface.h"
#include <stdio.h>
#include <string.h>

LivesInterface::LivesInterface(LivesOrigin origin){
	this->Origin = origin;
	strcpy(this->name, "unnamed live");
}

LivesInterface::LivesInterface(LivesOrigin origin, char* name){
	this->Origin = origin;
	strcpy(this->name, name);
}

LivesInterface::~LivesInterface(){
}

char* LivesInterface::getName(){
	return this->name;
}

LivesInterface::LivesOrigin LivesInterface::getOrigin(){
	return this->Origin;
}
LivesInterface::Position& LivesInterface::getPosition(){
	return this->position;
}
void LivesInterface::setPosition(Position pos){
	this->position = pos;
}

void LivesInterface::setPosition(float x, float y){
	Position pos;
	pos.x = x;
	pos.y = y;
}