#include "Sing_Up.h"

using namespace cocos2d::ui;
using namespace cocos2d;

cocos2d::Scene* Sing_Up::Sing_Up_scene(){
	cocos2d::Scene* Sing_Up_Scene = cocos2d::Scene::create();
	Sing_Up* R_Sing_Up = Sing_Up::create();
	Sing_Up_Scene->addChild(R_Sing_Up);
	return Sing_Up_Scene;
}

bool Sing_Up::init(){
	if (!cocos2d::Layer::init()) return false;

	//저장 버튼
	Save_Button = Button::create("background_textbox.png");
	Save_Button->setSwallowTouches(false);
	Save_Button->setPosition(cocos2d::Vec2(100, 300));
	Save_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	Save_Button->setTitleText("Save");
	this->addChild(Sing_Up::Save_Button);

	//취소 버튼
	cancel_Button = Button::create("background_textbox.png");
	cancel_Button->setSwallowTouches(false);
	cancel_Button->setPosition(cocos2d::Vec2(100, 100));
	cancel_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	cancel_Button->setTitleText("cancel");
	this->addChild(Sing_Up::cancel_Button);

	ID_EditBox = EditBox::create(cocos2d::Size(300, 50), "background_textbox.png");
	ID_EditBox->setFontSize(30.0f);
	ID_EditBox->setFontColor(cocos2d::Color3B(1, 1, 0));
	ID_EditBox->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f /2.0f ));
	this->addChild(ID_EditBox);

	Password_EditBox = EditBox::create(cocos2d::Size(300, 50), "background_textbox.png");
	Password_EditBox->setFontSize(30.0f);
	Password_EditBox->setFontColor(cocos2d::Color3B(1, 1, 0));
	Password_EditBox->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2.0f - 60.0f));
	this->addChild(Password_EditBox);

	auto Lamda_Maus_Touch = EventListenerTouchOneByOne::create();
	Lamda_Maus_Touch->onTouchBegan = std::bind(&Sing_Up::Touch_maus, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Lamda_Maus_Touch, this);

	return true;
}
bool Sing_Up::Touch_maus(cocos2d::Touch* Tch, cocos2d::Event* Et){
	Rect cancel_Rect = Sing_Up::cancel_Button->getBoundingBox();
	Rect Save_Rct = Sing_Up::Save_Button->getBoundingBox();
	if (Save_Rct.containsPoint(Tch->getLocation())){
		HelloWorld::User_Get_Id_Password_map.insert(std::pair 
			<std::string, std::string>(ID_EditBox->getText(), Password_EditBox->getText()));
		Director::getInstance()->replaceScene(HelloWorld::createScene());
	}
	if (cancel_Rect.containsPoint(Tch->getLocation())){
		Director::getInstance()->replaceScene(HelloWorld::createScene());
	}
	return true;
}