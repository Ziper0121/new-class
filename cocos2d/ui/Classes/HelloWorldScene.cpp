#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"

//그다음 이것을 사용하면 cocos2d::ui를 무시 할수있다.
using namespace cocos2d::ui;
using namespace cocos2d;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	ChackBox();
	return true;
}
void HelloWorld::button(){
	cocos2d::ui::Button* button01 = cocos2d::ui::Button::create("btn_up.png");
	button01->setTitleText("Button text!");
	button01->setTitleColor(Color3B(0, 0, 0));
	button01->setTitleFontSize(24.0f);
	button01->setPosition(Vec2(100, 200));
	this->addChild(button01);

	cocos2d::ui::Button* button02 = cocos2d::ui::Button::create("btn_up.png", "btn_down.png", "btn_disabled.png");
	button02->setTitleText("Button text 2 !");
	button02->setTitleColor(Color3B(255, 0, 0));
	button02->setTitleFontSize(20.0f);
	button02->setPosition(Vec2(300, 200));
	this->addChild(button02);

	cocos2d::ui::Button* button03 = cocos2d::ui::Button::create("btn_up.png", "btn_down.png", "btn_disabled.png");
	button03->setTitleText("Disabled !");
	button03->setTitleColor(Color3B(255, 255, 0));
	button03->setTitleFontSize(18.0f);
	button03->setPosition(Vec2(500, 200));
	button03->setEnabled(false);
	this->addChild(button03);
}
void HelloWorld::ChackBox(){
	cocos2d::ui::CheckBox* checkbox01 = cocos2d::ui::CheckBox::create("checkbox_background.png","checkbox_checked.png");
	checkbox01->setPosition(Vec2(100, 200));
	this->addChild(checkbox01);

	cocos2d::ui::CheckBox* checkbox02 = cocos2d::ui::CheckBox::create("checkbox_background.png","checkbox_down.png","checkbox_checked.png","checkbox_disabled.png","checkbox_checked_disabled.png");
	checkbox02->setPosition(Vec2(300, 200));
	this->addChild(checkbox02);

	cocos2d::ui::CheckBox* checkbox03 = cocos2d::ui::CheckBox::create("checkbox_background.png", "checkbox_down.png", "checkbox_checked.png", "checkbox_disabled.png", "checkbox_checked_disabled.png");
	checkbox03->setPosition(Vec2(500, 200));
	checkbox03->setEnabled(false);
	this->addChild(checkbox03);
}
void HelloWorld::TextField(){
	cocos2d::ui::TextField* textField = cocos2d::ui::TextField::create("Enter Text", "04b20.ttf", 24.0f);
	textField->setPosition(Vec2(600, 300));
	this->addChild(textField);
}

void HelloWorld::EditBox(){
	cocos2d::ui::EditBox* editBox01 = cocos2d::ui::EditBox::create(Size(500, 60), "editbox_background.png");
	editBox01->setPosition(Vec2(600, 200));
	editBox01->setFontSize(24.0f);
	this->addChild(editBox01);
}