#ifndef __Sing_Up__h_
#define __Sing_Up__h_
#include "HelloWorldScene.h"
#include <map>

class Sing_Up : public cocos2d::Layer{
public:
	//Read_Article의 씬 
	static cocos2d::Scene* Sing_Up_scene();
	CREATE_FUNC(Sing_Up);

	//몸체 ?
	virtual bool init();

	/*          변수          */
	//돌아가는 버튼
	cocos2d::ui::Button* Save_Button;
	cocos2d::ui::Button* cancel_Button;

	cocos2d::ui::EditBox* ID_EditBox;
	cocos2d::ui::EditBox* Password_EditBox;

	int User_number = 0;

	//버튼 터치
	bool Touch_maus(cocos2d::Touch* Tch, cocos2d::Event* Et);

};

#endif //__Sing_Up__h_