#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	Button* saveData = Button::create("btn_up.png");
	saveData->setPosition(Vec2(200, 320));
	saveData->setTitleText("Save ValueMap");
	saveData->setTitleColor(Color3B(0, 0, 0));
	saveData->addClickEventListener([](Ref* r)->void {
		auto fileutil = FileUtils::getInstance();
		std::string path = fileutil->getWritablePath();
		CCLOG("Writable path: %s", path.c_str());

		std::string saveFileFullpath = path + "myData.xml";
		CCLOG("save File-Full - Path: %s", saveFileFullpath);

		//==Prepare Save File
		ValueVector myItems = ValueVector();
		myItems.push_back(Value("Apple"));
		myItems.push_back(Value("Dagger"));
		myItems.push_back(Value("Short sword"));

		ValueMap saveData = ValueMap();
		saveData["My Level"] = Value(10);
		saveData["MyEsp"] = Value(2455);
		saveData["My Gold"] = Value(2522);
		saveData["My Status-Lucky"] = Value(32.125f);
		saveData["My Items"] = myItems;

		//==Write File
		fileutil->writeValueMapToFile(saveData, saveFileFullpath);
	});
	addChild(saveData);

	Button* LoadData = Button::create("btn_up.png");
	LoadData->setPosition(Vec2(400, 320));
	LoadData->setTitleText("Load ValueMap");
	LoadData->setTitleColor(Color3B(0, 0, 0));
	LoadData->addClickEventListener([](Ref* r)->void {
		auto fileutil = FileUtils::getInstance();
		std::string path = fileutil->getWritablePath();
		CCLOG("Writable path: %s", path.c_str());

		std::string saveFileFullpath = path + "myData.xml";
		CCLOG("save File Full - Path: %s", saveFileFullpath);

		//== Load File
		ValueMap dict = fileutil->getValueMapFromFile(saveFileFullpath);

		int myLevel = dict["myLevel"].asInt();
		int myExp = dict["MyExp"].asInt();
		int myGold = dict["myGold"].asInt();
		float myLucky = dict["MyStatus-Lucky"].asFloat();
		ValueVector myItems = dict["myItems"].asValueVector();

		CCLOG("My Level : %d", myLevel);
		CCLOG("My Exp : %d", myExp);
		CCLOG("My Gold : %d", myGold);
		CCLOG("My Lucky : %f", myLucky);
		int i = 0;
		for (auto it = myItems.begin(); it < myItems.end(); it++){
			std::string item = (*it).asString();
			CCLOG("My Items %d : %s", i++, item.c_str());
		}
	});
	addChild(LoadData);

    return true;
}
