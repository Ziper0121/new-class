#include <stdio.h>
#include "Header.h"

int main(){
	MyStack<float> stack = MyStack<float>();
	float top;

	stack.push(100.2);
	stack.push(200.5);
	top = stack.top();
	printf("MyStack, currentTop:&f\n", top);
	stack.pop();

	stack.push(129.2);
	stack.push(241202.1);
	stack.push(2120.666);
	printf("MyStack, currentTop:%f\n", stack.top());

	stack.pop();
	stack.pop();
	stack.pop();

	printf("MyStack, isEmpty:%d\n", stack.empty());
	return 0;
}