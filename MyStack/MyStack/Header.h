#include <cstdlib>

template <typename T>
class MyStack{
private:
	int topIndex = 0;
	T* stackList = nullptr;
	int capacity = 2;
public:
	MyStack(){
		stackList = (T*)malloc(sizeof(T)*capacity);
	}
	virtual ~MyStack(){
		free(stackList);
	}

	void push(T element){
		if (topIndex >= capacity){
			T* newStackList = (T*)malloc(sizeof(T)*capacity * 2);
			for (int i = 0; i < capacity; i++){
				newStackList[i] = stackList[i];
			}
			capacity = capacity * 2;
			free(stackList);
			stackList = newStackList;
		}
		stackList[topIndex++] = element;
	}

	T top(){
		return stackList[topIndex-1];
	}
	void pop(){
		topIndex--;
	}

	bool empty(){
		return topIndex == 0;
	}
};