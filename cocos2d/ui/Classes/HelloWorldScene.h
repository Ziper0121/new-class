#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
//ui를 사용하기 위해서는 이것이 필요하다.
#include "ui/CocosGUI.h"

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	//버튼의 이해
	void button();
	//checkBox의 이해
	void ChackBox();
	//TextField의 이해 웹용
	void TextField();
	//EditBox의 이해 디바이스용
	void EditBox();
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
