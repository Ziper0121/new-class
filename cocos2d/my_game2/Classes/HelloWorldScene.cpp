#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	HelloWorld::scren[0] = cocos2d::Sprite::create("batang1.png");
	HelloWorld::scren[1] = cocos2d::Sprite::create("batang2.png");
	for (int i = 0; i < 2; i++){
		HelloWorld::scren[i]->setPosition(330 + (480 * i), 320);
		this->addChild(HelloWorld::scren[i]);
	}

	HelloWorld::buten[0] = cocos2d::Sprite::create ("my.png");
	HelloWorld::buten[1] = cocos2d::Sprite::create("lo.png");
	HelloWorld::buten[2] = cocos2d::Sprite::create("st.png");

	for (int i = 0; i < 3; i++){
		HelloWorld::buten[i]->setPosition( 700+(100*i),600);
		this->addChild(HelloWorld::buten[i]);
	}

	HelloWorld::option[0] = cocos2d::Sprite::create("option.png");
	HelloWorld::option[1] = cocos2d::Sprite::create("start.png");

	for (int i = 0; i < 2; i++){
		HelloWorld::option[i]->setPosition(830,100+(100 * i));
		this->addChild(HelloWorld::option[i]);
	}

	HelloWorld::name = cocos2d::Sprite::create("name.png");
	HelloWorld::name->setPosition(300,600);
	this->addChild(HelloWorld::name);
    return true;
}
