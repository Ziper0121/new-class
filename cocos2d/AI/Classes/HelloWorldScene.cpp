#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	AI_1_Mario_Sprite = Sprite::create("mario1.png");
	AI_1_Mario_Sprite->setPosition(Vec2(800, 500));
	addChild(AI_1_Mario_Sprite);

	AI_2_Mario_Sprite = Sprite::create("mario1.png");
	AI_2_Mario_Sprite->setPosition(Vec2(100, 100));
	addChild(AI_2_Mario_Sprite);

	number_bullet = 0;

	AI_GetPosition[0] = AI_1_Mario_Sprite->getPosition();
	AI_GetPosition[1] = AI_2_Mario_Sprite->getPosition();

	schedule(schedule_selector(HelloWorld::AI_setPosition));

    return true;
}

void HelloWorld::AI_setPosition(float dt){
	Vec2 AI_GetPosition[2] = {AI_GetPosition[0], AI_GetPosition[1]};
	schedule(schedule_selector(HelloWorld::AI_echo_Choose), 5);

	for (int i = 0; i < 2; i++){
		if (AI_GetPosition[i].x > 950){
			AI_Run_Vec2[i] = (POS_RIGHT);
		}
		else if (AI_GetPosition[i].x < 0){
			AI_Run_Vec2[i] = (POS_LEFT);
		}
		if (AI_GetPosition[i].y > 630){
			AI_Run_Vec2[i] = (POS_BOTTOM);
		}
		else if (AI_GetPosition[i].y < 0){
			AI_Run_Vec2[i] = (POS_TOP);
		}
		AI_GetPosition[i] += (AI_Run_Vec2[i]);
	}

	AI_1_Mario_Sprite->setPosition(AI_GetPosition[0]);
	AI_2_Mario_Sprite->setPosition(AI_GetPosition[1]);
}

void HelloWorld::AI_echo_Choose(float dt){
	srand((unsigned int)time(NULL));
	int k = rand();
	switch (k % 4){
	case 0:
		AI_Run_Vec2[0] = (POS_TOP);
		break;
	case 1:
		AI_Run_Vec2[0] = (POS_LEFT);
		break;
	case 2:
		AI_Run_Vec2[0] = (POS_RIGHT);
		break;
	case 3:
		AI_Run_Vec2[0] = (POS_BOTTOM);
		break;
	}
	k = rand();
	switch (k % 4){
	case 0:
		AI_Run_Vec2[1] = (POS_TOP);
		break;
	case 1:
		AI_Run_Vec2[1] = (POS_LEFT);
		break;
	case 2:
		AI_Run_Vec2[1] = (POS_RIGHT);
		break;
	case 3:
		AI_Run_Vec2[1] = (POS_BOTTOM);
		break;
	}
}

void HelloWorld::AI_Bullet_make(float dt){
	for (int i = 0; i < 2; i++){
		AI_GetPosition[i] ;
		AI_1_bullet[i] = Sprite::create("mario3.png");
		AI_1_bullet[i]->setPosition(AI_GetPosition[i]);
		addChild(AI_1_bullet[i]);
	}
}
void HelloWorld::AI_Gun_Animaition(float dt){
	Get_taget_Position[0] = HelloWorld::AI_Target
		(AI_GetPosition[0], AI_GetPosition[1]);
	Get_taget_Position[1] = HelloWorld::AI_Target
		(AI_GetPosition[0], AI_GetPosition[1]);
	schedule(schedule_selector(HelloWorld::AI_Bullet_make), 2);


}
Vec2 HelloWorld::AI_Target(Vec2 Position_1, Vec2 Position_2){
	double AI_Difference[2] = { Position_1.x - Position_2.x, Position_1.y - Position_2.y };
	for (int i = 0; i < 2; i++){
		AI_Difference[i] = AI_Difference[i] * AI_Difference[i];
	}
	double return_Length = AI_Difference[0]+ AI_Difference[1];

	return return_Length;
}