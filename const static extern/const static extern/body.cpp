#include "Header.h"
#include <cstdio>

//외부 const의 값을 부르는 변수
void print_const_integer(){
	printf("Header.h > CONST_INTEGER : %d \n", CONST_INTEGER);
}
//class내부의 
char* ConstExampleClass::returnString_1(){
	return "normal string";
}
const char* ConstExampleClass::returnString_2(){
	return "const string";
}


//class내부에 있는 변수의 값을 초기화 외부의 static에는 값이 변경되지 않는다.
int StaticExampleClass::staticInteger = 15;
void StaticExampleClass::staticFunction(){
	printf("staitc function~!\n");
}
//외부의 static을 나타내는 함수
void print_static_integer(){
	printf("static.cpp > static : %d\n", staticInteger);
}
//class내부의 있는 static의 함수
void print_class_static_integer(){
	printf("static.cpp > StaticExampleClass::staticInteger : %d \n", 
		StaticExampleClass::staticInteger);
}
//estern의 값을 200으로 초기화
int externInteger = 200;
//몸체의 extern의 값을 반환한다.
void print_extern_integer(){
	printf("extern.cpp > externInteger : %d\n", externInteger);
}
