#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	//버튼
	HelloWorld::HomeButton = cocos2d::Sprite::create("mario.png");
	HelloWorld::HomeButton->setPosition(640 / 2, 960 / 2);
	this->addChild(HomeButton);

	//버튼을 받는 람다 변수
	cocos2d::EventListenerTouchOneByOne* Lamdaboutton = cocos2d::EventListenerTouchOneByOne::create();
	Lamdaboutton->onTouchBegan = std::bind(&HelloWorld::Touch_Button, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Lamdaboutton, this);

    return true;
}
