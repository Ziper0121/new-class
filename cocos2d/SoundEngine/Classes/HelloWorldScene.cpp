#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	//시작 버튼
	cocos2d::ui::Button* buttonPlay = cocos2d::ui::Button::create("background_textbox.png");
	buttonPlay->addClickEventListener([&](cocos2d::Ref* rf)->void{
		auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
		audio->playBackgroundMusic("bgm.mp3", false);
	});
	buttonPlay->setTitleText("Play Music");
	buttonPlay->setTitleColor(cocos2d::Color3B(0, 0, 0));
	buttonPlay->setPosition(cocos2d::Vec2(200, 300));
	addChild(buttonPlay);
	//끝내는 버튼
	cocos2d::ui::Button* buttonstop = cocos2d::ui::Button::create("background_textbox.png");
	buttonstop->addClickEventListener([&](cocos2d::Ref* rf)->void{
		auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
		audio->stopBackgroundMusic(true);
		CCLOG("paused");
	});
	buttonstop->setTitleText("stop Music");
	buttonstop->setTitleColor(cocos2d::Color3B(0, 0, 0));
	buttonstop->setPosition(cocos2d::Vec2(400, 320));
	addChild(buttonstop);
	//?
	cocos2d::ui::Button* buttonEffect = cocos2d::ui::Button::create("background_textbox.png");
	buttonEffect->addClickEventListener([&](cocos2d::Ref* rf)->void{
		auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
		audio->playEffect("smb2_jump.wav");
	});
	buttonEffect->setTitleText("Play Effect");
	buttonEffect->setTitleColor(cocos2d::Color3B(0, 0, 0));
	buttonEffect->setPosition(cocos2d::Vec2(600, 320));
	addChild(buttonEffect);


    return true;
}
