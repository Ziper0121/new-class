#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	HelloWorld::mushroom = Sprite::create("stand.png");
	mushroom->setPosition(Vec2(200,300));
	this->addChild(mushroom);

	auto Key_boad = EventListenerKeyboard::create();
	Key_boad->onKeyPressed = std::bind(&HelloWorld::Pressed_key_boad, this, std::placeholders::_1, std::placeholders::_2);
	Key_boad->onKeyReleased = std::bind(&HelloWorld::Released_key_boad, this, std::placeholders::_1, std::placeholders::_2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(Key_boad, this);

	this->schedule(schedule_selector(HelloWorld::Move_Mushroom)); 
	this->schedule(schedule_selector(HelloWorld::Jump_Logic));

		return true;
}
void HelloWorld::Pressed_key_boad(EventKeyboard::KeyCode KCod, Event* Et){
	Animation* AniMushroom = Animation::create();
	if (KCod == EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		HelloWorld::LEFT_bool = true;
		mushroom->setFlippedX(false);
		AniMushroom->addSpriteFrameWithFile("run1.png");
		AniMushroom->addSpriteFrameWithFile("run2.png");
		AniMushroom->addSpriteFrameWithFile("run3.png");
		AniMushroom->addSpriteFrameWithFile("mario2.png");
	}
	if (KCod == EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		HelloWorld::RIGHT_bool = true;
		mushroom->setFlippedX(true);
		AniMushroom->addSpriteFrameWithFile("run1.png");
		AniMushroom->addSpriteFrameWithFile("run2.png");
		AniMushroom->addSpriteFrameWithFile("run3.png");
		AniMushroom->addSpriteFrameWithFile("mario2.png");
	}
	if (KCod == EventKeyboard::KeyCode::KEY_SPACE){
		HelloWorld* world = this;
		this->runAction(
			Sequence::create(
			DelayTime::create(0.1f),
			CallFunc::create([world]()->void{world->force = 50.0f; }
		), nullptr));
	}
	AniMushroom->setDelayPerUnit(0.1f);
}
void HelloWorld::Released_key_boad(EventKeyboard::KeyCode KCod, Event* Et){
	if (KCod == EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		HelloWorld::LEFT_bool = false;
	}
	if (KCod == EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		HelloWorld::RIGHT_bool = false;
	}

}

void HelloWorld::Move_Mushroom(float dt){
	Vec2 get_Mushrooom_Vec2 = mushroom->getPosition();
	if (HelloWorld::LEFT_bool){
		get_Mushrooom_Vec2.x -= dt * MOVE_OFFSET;
	}
	if (HelloWorld::RIGHT_bool){
		get_Mushrooom_Vec2.x += dt * MOVE_OFFSET;
	}

	mushroom->setPosition(get_Mushrooom_Vec2);
}
void HelloWorld::Jump_Logic(float dt){
	Animation* AniMushroom = Animation::create();
	cocos2d::Vec2 pos = mushroom->getPosition();
	force -= dt*MOVE_OFFSET;
	pos.y += force;
	if (pos.y >= 300.0f){
		AniMushroom->addSpriteFrameWithFile("jump.png");
		mushroom->setPosition(pos);
	}
	else if (pos.y < 300.0f && mushroom->getPosition().y != 300.0f){
		mushroom->setPosition(pos.x, 300.0f);
	}
	mushroom->runAction(RepeatForever::create(Animate::create(AniMushroom)));
}