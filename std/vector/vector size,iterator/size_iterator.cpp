#include <stdio.h>
#include <vector>

int main(){
	std::vector<int> vInt = std::vector<int>();
	vInt.push_back(200);
	vInt.push_back(300);
	
	for (int i = 0; i < 2; i++){
		printf("using random access(vInt[i]): %d\n ", vInt[i]);
	}
	for (std::vector<int>::const_iterator i = vInt.begin(); i != vInt.end(); i++){
		printf("using iterator(*i):%d\n", (*i));
	}
	for (int i = 0; i < vInt.size(); i++){
		printf("using size() to get size : %d\n", vInt.at(i));
	}
		return 0;
}
