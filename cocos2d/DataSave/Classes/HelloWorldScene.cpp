#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;


Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	EditBox* edString = EditBox::create(Size(400, 60), "editbox_background.png");
	edString->setPosition(Vec2(600, 320));
	addChild(edString);

	CheckBox* cdAudio = CheckBox::create("checkbox_background.png", "checkbox_checked.png");
	Label* ldAudio = Label::createWithTTF("Check", "04B_20__.TTF", 8.0f);
	ldAudio->setColor(Color3B(0, 0,0));
	ldAudio->setPosition(cdAudio->getBoundingBox().size / 2 + Size(24, 0));
	cdAudio->addChild(ldAudio);
	cdAudio->setPosition(Vec2(300, 480));
	addChild(cdAudio);

	Button* btnSave = Button::create("btn_up.png");
	btnSave->setTitleText("Load data");
	btnSave->setPosition(Vec2(100, 320));
	btnSave->setTitleColor(Color3B(0, 0, 0));
	addChild(btnSave);

	Button* btnLoad = Button::create("btn_up.png");
	btnLoad->setTitleText("Load data");
	btnLoad->setPosition(Vec2(300, 320));
	btnLoad->setTitleColor(Color3B(0, 0, 0));
	addChild(btnLoad);
	
	btnSave->addClickEventListener([edString, cdAudio](Ref* ref){
		UserDefault::getInstance()->setStringForKey("myString", edString->getText());
		UserDefault::getInstance()->setBoolForKey("myBoolean", cdAudio->isSelected());
		UserDefault::getInstance()->flush();
	});

	btnLoad->addClickEventListener([edString, cdAudio](Ref* ref){
		std::string mystring = UserDefault::getInstance()->getStringForKey("myString", "def");
		bool myBoolean = UserDefault::getInstance()->getBoolForKey("myBoolean", false);

		edString->setText(mystring.c_str());
		cdAudio->setSelected(myBoolean);
	});

    return true;
}
