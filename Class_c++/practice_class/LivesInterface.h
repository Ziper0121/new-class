class LivesInterface{
public :
	enum LivesOrigin{ ASIA=0,AFRICA,EUROPE,AMERICA,EVERYWERE,UNKNOWN};

	class Position{
	public:
		float x = 0.0f;
		float y = 0.0f;
	};
protected:
	char name[100];

	LivesOrigin Origin = UNKNOWN;

	Position position;

public:
	LivesInterface(LivesOrigin origin);

	LivesInterface(LivesOrigin origin, char* name);
	virtual~LivesInterface();

	char* getName();

	LivesOrigin getOrigin();

	Position& getPosition();

	void setPosition(Position pos);
	void setPosition(float x, float y);

	virtual void printToConsloe();

	virtual const char* getSpeciesString() = 0;
};