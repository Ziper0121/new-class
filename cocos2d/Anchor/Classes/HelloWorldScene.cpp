#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	mario = cocos2d::Sprite::create("mario.png");
	mario->setPosition(100.0f, 250.0f);
	mario->setAnchorPoint(Vec2(0.5f, 0.0f));
	mario->runAction(RepeatForever::create(RotateBy::create(1.0f, 350.0f)));
	this->addChild(mario);

	bird = cocos2d::Sprite::create("bird.png");
	bird->setPosition(300.0f, 250.0f);
	bird->setAnchorPoint(Vec2(0.5f, 0.0f));
	bird->runAction(RepeatForever::create(RotateBy::create(1.0f, 350.0f)));
	this->addChild(bird);

	cocos2d::Label* mairo_print = Label::createWithTTF("MARIO", "04B_21__.TTF", 24.0f);
	mairo_print->setPosition(Director::getInstance()->getOpenGLView()->getDesignResolutionSize() / 2);
	this->addChild(mairo_print);
	schedule(schedule_selector(HelloWorld::logic));
    return true;
}
void HelloWorld::logic(float dt){
	time = time + dt;
	Color3B color = mairo_print->getColor();

	if ()
}
