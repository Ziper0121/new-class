#include <stdio.h>
#include <stack>

#define tStack_float std::stack<Float>
typedef std::stack<int> tStack_int;
void SINT(){
	tStack_int sInt = tStack_int();
	sInt.push(10);
	sInt.push(20);
	sInt.push(13);
	sInt.push(20);
	printf("stack::top() : %d\n", sInt.top());
	printf("stack::size() : %d\n", sInt.size());
	sInt.pop();
	printf("stack::top() : %d\n", sInt.top());
	sInt.pop();
	sInt.pop();
	printf("stack::top() : %d\n", sInt.top());
	printf("stack::empty() : %d\n", sInt.empty());
	sInt.pop();
	printf("stack::empty() : %d\n", sInt.empty());
}