#include "Writing.h"

cocos2d::Scene* Writing::Writing_Scene(){
	cocos2d::Scene* Writing_Scene = cocos2d::Scene::create();
	Writing* R_Writing = Writing::create();
	Writing_Scene->addChild(R_Writing);
	return Writing_Scene;
}

bool Writing::init(){
	if (!cocos2d::Layer::init()) return false;

	//저장 버튼
	Writing::Save_Button = cocos2d::ui::Button::create("background_textbox.png");
	Writing::Save_Button->setSwallowTouches(false);
	Writing::Save_Button->setPosition(cocos2d::Vec2(100, 300));
	Writing::Save_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	Writing::Save_Button->setTitleText("Save");
	this->addChild(Writing::Save_Button);

	//취소 버튼
	Writing::cancel_Button = cocos2d::ui::Button::create("background_textbox.png");
	Writing::cancel_Button->setSwallowTouches(false);
	Writing::cancel_Button->setPosition(cocos2d::Vec2(100, 100));
	Writing::cancel_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	Writing::cancel_Button->setTitleText("cancel");
	this->addChild(Writing::cancel_Button);

	Writing::User_Get_Writing = cocos2d::ui::EditBox::create(cocos2d::Size(500,100),"background_textbox.png");
	Writing::User_Get_Writing->setFontSize(30.0f);
	Writing::User_Get_Writing->setFontColor(cocos2d::Color3B(1, 1, 0));
	Writing::User_Get_Writing->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f - 140.0f));
	this->addChild(Writing::User_Get_Writing);

	Writing::User_Get_title = cocos2d::ui::EditBox::create(cocos2d::Size(500, 500), "background_textbox.png");
	Writing::User_Get_title->setFontSize(30.0f);
	Writing::User_Get_title->setFontColor(cocos2d::Color3B(1, 1, 0));
	Writing::User_Get_title->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f/2.0f - 140.0f));
	this->addChild(Writing::User_Get_title);

	auto Lamda_Maus_Touch = cocos2d::EventListenerTouchOneByOne::create();
	Lamda_Maus_Touch->onTouchBegan = std::bind(&Writing::Touch_echo, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Lamda_Maus_Touch, this);

	return true;
}
bool Writing::Touch_echo(cocos2d::Touch* Tch, cocos2d::Event* Et){
	cocos2d::Rect cancel_Rect = Writing::cancel_Button->getBoundingBox();
	cocos2d::Rect Save_Rct = Writing::Save_Button->getBoundingBox();
	if (Save_Rct.containsPoint(Tch->getLocation())){
		Article_List::Article_name.push_back(Writing::User_Get_Writing->getText());
		Article_List::Article_in.push_back(Writing::User_Get_title->getText());
		cocos2d::Director::getInstance()->replaceScene(Article_List::Article_List_Scene());
	}
	if (cancel_Rect.containsPoint(Tch->getLocation())){
		cocos2d::Director::getInstance()->replaceScene(Article_List::Article_List_Scene());
	}
	return true;
}