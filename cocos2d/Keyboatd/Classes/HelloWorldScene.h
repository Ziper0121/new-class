#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#define FORCE_DECREASING_RATIO 90
#define MOVE_OFFSET 200
class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	float force = 0.0f;
	bool leftPressing = false;
	bool rightPressing = false;
	bool spase = false;
	cocos2d::Sprite* bird = nullptr;
	//cocos2d::EventKeyboard::KeyCode  Ű�� ������ ��
	void Keypressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* evt);
	//cocos2d::EventKeyboard::KeyCode  Ű�� ������
	void KeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* evt);

	void jumpLogic(float dt);
	void logic(float dt); 

    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
