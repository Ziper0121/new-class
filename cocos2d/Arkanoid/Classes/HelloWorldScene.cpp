#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}

	createBlocks();
	createPlayerSprite();
	createBallSprtie();
	registKeyboardEvent();

	schedule(schedule_selector(HelloWorld::schedulePlayerLogic));

    return true;
}

// == 오브젝트를 생산하는 함수들
void HelloWorld::createPlayerSprite(){
	if (player != nullptr) return;
	player = Sprite::create("bar-1.png");
	player->setPosition(PLAYER_INIT_POS);
	addChild(player);
}
void HelloWorld::createBallSprtie(){
	if (ball != nullptr) return;
	if (player == nullptr) return;
	ball = Sprite::create("mball.png");
	ball->setPosition(PLAYER_INIT_POS + Vec2(0, tileWidth));
	addChild(ball);
}

void HelloWorld::createBlocks(){
	for (int i = 0; i < BLOCK_VERTICAL_MAX; i++){
		for (int k = 0; k < BLOCK_HORIZONTAL_MAX; k++){
			Sprite* blocks_create = blocks[i][k];
			switch (i){
			case 5:
				HelloWorld::reinforce_blocks(blocks_create, "block-blue-1.png", i, i, k);
				continue;
			case 4:
				HelloWorld::reinforce_blocks(blocks_create, "block-green-1.png", i, i, k);
				continue;
			case 3:
				HelloWorld::reinforce_blocks(blocks_create, "block-red-1.png", i, i, k);
				continue;
			case 2:
				HelloWorld::reinforce_blocks(blocks_create, "block-pink-1.png", i, i, k);
				continue;
			case 1:
				HelloWorld::reinforce_blocks(blocks_create, "block-orange-1.png", i, i, k);
				continue;
			case 0:
				HelloWorld::reinforce_blocks(blocks_create, "block-orange-1.png", i, i, k);
				continue;
			}
		}
	}
}

// == 키보드로 움직임을 제어하는 함수들
void HelloWorld::registKeyboardEvent(){
	if (this->keybd != nullptr)return;

	keybd = EventListenerKeyboard::create();
	keybd->onKeyPressed = [&](EventKeyboard::KeyCode Keycode, Event* e)->void {
		switch (Keycode){
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			leftArrow = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			rightArrow = true;
			break;
		}
	};
	keybd->onKeyReleased = [&](EventKeyboard::KeyCode Keycode, Event* e)->void{
		switch (Keycode){
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			leftArrow = false;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			rightArrow = false;
			break;
		case EventKeyboard::KeyCode::KEY_SPACE:
			if (state == READY)
				//시작을 할수있게 해주는 함수
				setStatePlaying();
			break;
		}
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keybd, this);
}

// == 나아갈 반향과 속도를 잡는 함수
void HelloWorld::makeDirectionVectorByAngle(float speed, float angle){
	this->ballAngle = angle;
	ballDirection = Vec2(speed* cos(angle), speed* sin(angle));
}

// == 키보드의 값을 받고 속도를 변경해주는 스케줄
void HelloWorld::schedulePlayerLogic(float dt){
	if (player == nullptr) return;

	//==Player좌우 이동
	//==READY 상태 이거나 PLAYING 상태 일 경우에만 플레이어가 좌우로 이동한다.
	if (state == READY || state == PLAYING){
		Vec2 Playerpos = player->getPosition();
		Vec2 pos;
		if (leftArrow){
			pos = PLAYER_MOVING_DT * -dt;
		}
		if (rightArrow){
			pos = PLAYER_MOVING_DT * +dt;
		}

		// == 화면 밖으로 나가지 않게 처리해줌
		if (Playerpos.x + pos.x < 0)return;
		if (Playerpos.x + pos.x > AppDelegate::displaySize.width)return;

		//== 바뀐 포지션을 등록 
		player->setPosition(player->getPosition() + pos);

		//==READY 상태일 경우 공도 함께 움직게 해준다.
		if (state == READY){
			ball->setPosition(ball->getPosition() + pos);
		}
	}
}

//makeDirectionVectorByAngle(ballSpeed, -ballAngle)는 반향을 반대로 해주게 하는 것
// == 벽과 블럭과 플레이어의 충돌을 감지하는 스케줄러
void HelloWorld::scheduleBallMoving(float dt){
	if (ball == nullptr) {
		CCLOG("Ball Sprite is nullptr");
		return;
	}
	if (state != PLAYING){
		CCLOG("Ball Sptie is not playing state");
		return;
	}
	Vec2 pos = ball->getPosition();
	Vec2 moveto = pos + ballDirection* dt;
	//TODO: 이공에서 pos 를 이용해 충돌 감지 처리 할것.
	//좌우벽 충돌 감지
	if (moveto.x > AppDelegate::displaySize.width || moveto.x < 0){
		makeDirectionVectorByAngle(ballSpeed, PI-ballAngle);
		moveto = pos + ballDirection* dt;
	}
	//상 벽 충돌 감지 하벽은 아웃을 시킬 것이기 때문에 따로 구현
	else if (moveto.y > AppDelegate::displaySize.height){
		makeDirectionVectorByAngle(ballSpeed, -ballAngle);
		moveto = pos + ballDirection* dt;
	}
	//하 벽 충돌 감지
	else if (moveto.y < 0){
		setStateLose();
	}
	//플레이어와의 충돌 감지
	else if (player->getBoundingBox().containsPoint(moveto)){
		CCLOG("moveto.x : % 2f, moveto.y : % 2f", moveto.x, moveto.y);
		makeDirectionVectorByAngle(ballSpeed, -ballAngle);
		moveto = pos + ballDirection* dt;
		CCLOG("moveto.x : % 2f, moveto.y : % 2f", moveto.x, moveto.y);
	}
	//== 블록들과의 충돌 감지
	for (int i = 0; i < BLOCK_VERTICAL_MAX; i++){
		for (int k = 0; k < BLOCK_HORIZONTAL_MAX; k++){
			if (blocks[i][k] == nullptr){
			}
			// == 블록과 공이 같은 위치에 있을 경우
			else if (draWcircle(moveto,12, i,k)){
				Vec2 blockPos = blocks[i][k]->getPosition();
				//pos = 현재 위치
				//moveto = 옯겨갈 위치
				//block pos = 블록의 중점
				//블록의 중점과 현재 공의 위치를 알면 입사각을 구할 수 있다.
				//블록은 가로길이 16 , 세로길이 8의 직사각형.
				// 직선경로에 다른 블록 꼭지점을 무시하는 경우가 있어 완벽하게 구현되는 않음.
				float angleBase = atan2(4, 8); //블록 각 1사분면 최초
				float angleBaseInv = (PI* 0.5f) - angleBase; // 1사문면 블록 1사분면 이우 90도 까지의 각

				// angleBase + angleBaseInv = 0*5PI 의 각이 나옴.

				Vec2 delta = blockPos - pos;
				float angle = delta.getAngle();

				//오른쪽변에 충돌시
				if (angle> DEG_270 + angleBaseInv || moveto.x < 0){
					makeDirectionVectorByAngle(ballSpeed, PI - ballAngle);
				}
				else if (angle >= angleBaseInv&& angle < DEG_90 + angleBaseInv){
					makeDirectionVectorByAngle(ballSpeed, -ballAngle);
				}
				else if (angle >= DEG_90 + angleBaseInv && angle < DEG_180 + angleBase){
					makeDirectionVectorByAngle(ballSpeed, PI - ballAngle);
				}
				//하단벽에 충돌시
				else {
					makeDirectionVectorByAngle(ballSpeed, -ballAngle);
				}
				moveto = pos + ballDirection* dt;
				
				// == 여기서부터 새로 작성
				blockHealth[i][k] -= ball_Damage;

				if (blockHealth[i][k]  <= 0){
					Sprite* taget = blocks[i][k];
					Animation* anim = Animation::create();
					for (int i = 1; i <= 7; i++){
						std::stringstream ss;
						ss << "block-orange-"<< i << ".png";
						anim->addSpriteFrameWithFile(ss.str());
					}
					anim->setDelayPerUnit(0.04);
					taget->runAction(
						Sequence::create(
						Animate::create(anim),
						RemoveSelf::create(),
						nullptr));
					blocks[i][k] = nullptr;
				}
				if (blockHealth[i][k] == 1){
					HelloWorld::destroyBlock(i, k, "orange-", "block-orange-1.png");
				}
				if (blockHealth[i][k] == 2){
					HelloWorld::destroyBlock(i, k, "pink-", "block-orange-1.png");
				}
				if (blockHealth[i][k] == 3){
					HelloWorld::destroyBlock(i, k, "red-", "block-pink=1.png");
				}
				if (blockHealth[i][k] == 4){
					HelloWorld::destroyBlock(i, k, "green-", "block-red-1.png");
				}
				if (blockHealth[i][k] == 5){
					HelloWorld::destroyBlock(i, k, "blue-", "block-green-1.png");
				}
				break;
			}
		}
	}
	if (ball != nullptr)ball->setPosition(moveto);
}

//== Playing 은 Ready 상태에서만 갈 수 있다.
void HelloWorld::setStatePlaying(){
	if (state == GameState::READY){
		state = GameState::PLAYING;
		CCLOG("Game state Ready -> Playing");
		makeDirectionVectorByAngle(INIT_SPEED, 1.0f);
		schedule(schedule_selector(HelloWorld::scheduleBallMoving));
	}
}
//==lose는 playing 상태에서만 갈 수 있다.
void HelloWorld::setStateLose(){
	if (state == PLAYING){
		CCLOG("Game state Playing -> lose");
		unschedule(schedule_selector(HelloWorld::scheduleBallMoving));
		Label* lblose = Label::createWithTTF("Wou Lose", "04B_21__.ttf", 20.0f);

		lblose->setOpacity(0);
		lblose->setPosition(AppDelegate::displaySize / 2);
		lblose->runAction(Sequence::create(FadeIn::create(0.5f), DelayTime::create(1.0f)
			, FadeOut::create(0.2f), RemoveSelf::create(), nullptr));
		addChild(lblose);
		ball->removeFromParent();
		ball = nullptr;
		player->removeFromParent();
		player = nullptr;
	}
	else{
		CCLOG("상태 정이 예외 발생  setStateLose()");
	}
}

//== Win 은 Playing 상태에서만 갈 수 있다.
void HelloWorld::setStateWin(){
	if (state == PLAYING){
		CCLOG("Game state PLAYING->WIn");
		unschedule(schedule_selector(HelloWorld::scheduleBallMoving));
	}
	else {
		CCLOG("상태 전이 예외 발생 , setStateWin()");
	}
}

// == 블럭을 설정하고 위치를 정해주는 함수
void HelloWorld::reinforce_blocks(Sprite* Blocks_Sprite , char* Blocks_color,int HP, int x, int y){
	blocks[x][y] = Sprite::create(Blocks_color);
	blocks[x][y]->setPosition(y * 16 + 8, x * 8 + AppDelegate::displaySize.height - 48 + 4);
	addChild(blocks[x][y]);

	//==블록의 체력을 설정함
	blockHealth[x][y] = HP+1;
}

void HelloWorld::destroyBlock(int i, int k, char* now, char* after ){
	Sprite* taget = blocks[i][k];
	Animation* anim = Animation::create();
	for (int i = 1; i <= 7; i++){
		std::stringstream ss;
		ss << "block-" << now << i << ".png";
		anim->addSpriteFrameWithFile(ss.str());
	}
	anim->addSpriteFrameWithFile(after);
	anim->setDelayPerUnit(0.04);
	taget->runAction(
		Sequence::create(
		Animate::create(anim),
		nullptr));
}

bool HelloWorld::draWcircle(Vec2 ballPos, int segment,int i, int k){
	Rect draWcircle = blocks[i][k]->getBoundingBox();
	float r = 4;
	for (int i = 0; i < segment; i++){
		Vec2 point = Vec2(
			r*cos((DEG_360 / segment)*i),
			r*sin((DEG_360 / segment)*i))
			+ballPos;
		if (draWcircle.containsPoint(point)){
			return true;
		}
	}
	return false;
}