#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	//원을 만드는 것
	void draWCircle();
	//마름모 만드는 것
	void draWpoly();

	//그리드 만들기
	void grid();
	//삼각함수로 원의 색깔변화를 그리는 것
	void samgag();
	//삼각형 만들기
	void triangle();
	//다각형 만들기
	void square();
	//태극기
	void KoreanFlag();
	//오만성
	void ohmanseong();
	//움직이는 태극기
	void runFlag(float dt);
	//작은 원
	void drawCircle();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
