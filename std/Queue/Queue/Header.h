#include <cstdlib>

template <typename T>
class MyQueue {
private :
	int Que = 0;
	int QueueIndex = 0;
	T* QueueList = nullptr;
	int capacity = 2;
public :
	MyQueue(){
		QueueList = (T*)malloc(sizeof(T)*capacity);
	}
	virtual ~MyQueue(){
		free(QueueList);
	}
	void push(T element){
		if (QueueIndex >= capacity){
			T* newQueueList = (T*)malloc(sizeof(T)*capacity*2);
			for (int i = 0; i < capacity; i++){
				newQueueList[i] = QueueList[i];
			}
			capacity = capacity * 2;
			free(QueueList);
			QueueList = newQueueList;
		}
		QueueList[QueueIndex++] = element;
	}

	T top(){
		return QueueList[Que];
	}

	void pop(){
		Que++;
	}

	bool empty(){
		return QueueIndex == Que;
	}
};