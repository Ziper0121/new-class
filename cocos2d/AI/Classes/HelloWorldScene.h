#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

//AI의 속도
#define MOVE_SPPED 1.0
using namespace cocos2d;

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	/*          변수          */
	//총알의 번호를 지정
	int number_bullet ;

	//첫번째 AI
	Sprite* AI_1_Mario_Sprite;
	//첫번째 AI 체력
	cocos2d::Label* AI_1_HP_Label;
	
	//두번째 AI
	Sprite* AI_2_Mario_Sprite;
	//두번째 AI 체력
	cocos2d::Label* AI_2_HP_Label;

	// == 총알을 만드는 Sprite
	Sprite* AI_1_bullet[6];

	//AI의 움직임을 담당하는 변수
	Vec2 AI_Run_Vec2[2];

	//AI의 좌표를 받는 변수
	Vec2 Get_taget_Position[2];

	//
	Vec2 AI_GetPosition[2];

	// == position의 반향을 부여해주는 vec2
	const Vec2 POS_TOP = Vec2(0, MOVE_SPPED);
	const Vec2 POS_LEFT = Vec2(MOVE_SPPED, 0);
	const Vec2 POS_RIGHT = Vec2(-MOVE_SPPED, 0);
	const Vec2 POS_BOTTOM = Vec2(0, -MOVE_SPPED);


	/*          함수          */
	// == AI가 지속적으로 적의 위치를 받는 함수 
	Vec2 AI_Target(Vec2 AI_1, Vec2 AI_2);

	// == AI의 총알을 지속적으로 만드는 함수
	void AI_Bullet_make(float dt);

	// == AI가 총을 쏘게 해주는 함수
	void AI_Gun_Animaition(float dt);

	// == AI를 이동 시키는 함수
	void AI_setPosition(float dt);

	// == AI의 반향을 바꾸어 주는 함수
	void AI_echo_Choose(float dt);

};

#endif // __HELLOWORLD_SCENE_H__
