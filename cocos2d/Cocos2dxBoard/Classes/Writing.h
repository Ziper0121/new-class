#ifndef __Writing_H__	
#define __Writing_H__
#include "Article_List.h"

class Writing : public cocos2d::Layer{
public:
	//Writing의 씬
	static cocos2d::Scene* Writing_Scene();
	CREATE_FUNC(Writing);

	//몸체?
	virtual bool init();

	/*          변수          */
	//취소 버튼
	cocos2d::ui::Button* cancel_Button;
	//저장 버튼
	cocos2d::ui::Button* Save_Button;

	//제목 쓰기
	cocos2d::ui::EditBox* User_Get_title;
	//글 쓰기 
	cocos2d::ui::EditBox* User_Get_Writing;

	/*          함수          */
	//행동의 방향을 잡아주는 함수
	bool Touch_echo(cocos2d::Touch* Tch, cocos2d::Event* Et);
};

#endif // __Writing_H__