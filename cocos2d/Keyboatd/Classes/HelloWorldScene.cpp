#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	bird = cocos2d::Sprite::create("bird1.png");
	bird->setPosition(960.0f / 2.0f, 640.0f / 2.0f);
	bird->setAnchorPoint(Vec2(0.5f, 0));
	this->addChild(bird);

	auto keybd = cocos2d::EventListenerKeyboard::create();
	//키보드가 눌리면
	keybd->onKeyPressed = std::bind(&HelloWorld::Keypressed, this, std::placeholders::_1, std::placeholders::_2);
	//키보드가 띄어지면
	keybd->onKeyReleased = std::bind(&HelloWorld::KeyReleased, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(keybd, this);
	this->schedule(schedule_selector(HelloWorld::logic));
	this->schedule(schedule_selector(HelloWorld::jumpLogic));
    return true;
}
//한번씩 검사를 하는 것
void HelloWorld::logic(float dt){
	cocos2d::Vec2 pos = bird->getPosition();
	if (leftPressing){
		pos.x = pos.x - dt*MOVE_OFFSET;
	}
	if (rightPressing){
		pos.x = pos.x + dt*MOVE_OFFSET;
	}
	bird->setPosition(pos);
}

void HelloWorld::Keypressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* evt){
	if (keyCode == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		leftPressing = true;
		bird->setFlippedX(true);
	}
	if (keyCode == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		rightPressing = true;
		bird->setFlippedX(false);
	}
	if (keyCode == cocos2d::EventKeyboard::KeyCode::KEY_SPACE){
		HelloWorld* world = this;
		bird->runAction(Sequence::create(
			ScaleTo::create(0.1f, 1.2f, 0.4f),
			ScaleTo::create(0.1f, 0.8f, 1.2f),
			ScaleTo::create(0.1f, 1.0f), nullptr));
		this->runAction(
			Sequence::create(
			DelayTime::create(0.2f),
			CallFunc::create([world]()->void{world->force = 30.0f; }
		), nullptr));
	}
}
void HelloWorld::KeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* evt){
	if (keyCode == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		rightPressing = false;
	}
	if (keyCode == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		leftPressing = false;
		bird->setFlippedX(false);
	}
}
//공이 튀기는 것
void HelloWorld::jumpLogic(float dt){
	cocos2d::Vec2 pos = bird->getPosition();
	force = force - (dt)*FORCE_DECREASING_RATIO;
	pos.y = pos.y + force;
	if (pos.y >= 640.0f / 2.0f){
		bird->setPosition(pos);
	}
	else if (pos.y < 640.0f / 2.0f && bird->getPosition().y != 640.0f / 2.0f){
		bird->setPosition(pos.x, 640.0f / 2.0);
	}
}