#ifndef __Article_List_H__
#define __Article_List_H__
//기본 해더
#include "HelloWorldScene.h"
//C++꺼
//내가 만든 해더
#include "Writing.h"
#include "Read_Article.h"

class Article_List : public cocos2d::Layer{
public:
	//List의 씬
	static cocos2d::Scene*  Article_List_Scene();
	CREATE_FUNC(Article_List);

	virtual bool init();
	/*          변수          */
	//글쓰기 이동
	cocos2d::ui::Button* Writing_Go_Button;
	//로그인으로 이동
	cocos2d::ui::Button* Login_Back_Button;

	std::vector<cocos2d::ui::Button*>  Button_make;

	//글 
	static std::vector<std::string> Article_name;
	static std::vector<std::string> Article_in;


	/*          함수          */
	//터치를 받아 이동하는 것
	bool Article_Bach_Touch(cocos2d::Touch* Tch, cocos2d::Event* Et);
};
#endif