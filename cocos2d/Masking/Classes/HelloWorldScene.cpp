#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	ClippingNode* clipping = ClippingNode::create(Node::create());
	clipping->setAlphaThreshold(0.0f);
	this->addChild(clipping);

	Node* bgNode = Node::create();
	clipping->addChild(bgNode);

	/*set stencil Node*/
	Sprite* sprite1 = Sprite::create("background.jpg");
	sprite1->setPosition(480, 320);

	bgNode->addChild(sprite1);

	/* Set Stencil Node */
	Sprite* stencilSprte = Sprite::create("masker.png");
	stencilSprte->setPosition(480, 320);

	clipping->getStencil()->addChild(stencilSprte);
    return true;
}
