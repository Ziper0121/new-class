#include <stdio.h>
#include <queue>

#define tStack_float std::queue<Float>
typedef std::queue<int> tStack_int;
void init_queue(){
	std::queue<int> qInt = std::queue < int >();
	qInt.push(10);
	qInt.push(30);
	qInt.push(13);
	qInt.push(20);
	printf(" qInt::top() : %d\n", qInt.front());
	qInt.pop();
	printf(" qInt::top() : %d\n", qInt.front());
	qInt.pop();
	qInt.pop();
	printf(" qInt::top() : %d\n", qInt.front());
	printf(" qInt::empty() : %d\n", qInt.empty());
	qInt.pop();
	printf(" qInt::empty() : %d\n", qInt.empty());
}